
module obstacle_generator_tb(score):

    output [1:0] score;

    reg clk;
    reg rstn;
    reg [1:0] pre;
    wire [1:0] out;
    reg [15:0] penalty;

    obstacle_generator obg(clk, rstn, pre, out);

    initial begin
        clk = 0;
        rstn = 0;
        pre = 0;
        penalty = 0;
        score = 0;
    end

    begin
        t = 0;
        sum = 0;
        while (t < 100)
            begin
                t <= t+1;
                clk <= ~clk;
                penalty <= penalty + ((pre + out == 3) ? 100 : 0);
                pre <= out;
                sum <= sum + (out[0] ^ out[1]);
            end
        penalty = penalty + ((sum < 20)?50:0)
        penalty = penalty + ((sum > 80)?50:0)
        score = ((penalty > 100) ? 0 : (100 - penalty));
    end

endmodule
