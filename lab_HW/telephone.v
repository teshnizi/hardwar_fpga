module telephone(call,pay,cancel,money,clk,inputmoney,idle,reset,state,callstate);

input wire call,clk,pay,cancel,reset;
input wire [1:0] money;

output reg [1:0] state;
output reg [1:0] callstate;
output reg [1:0] inputmoney;
output reg idle;

always @(posedge clk) begin
	if (reset) begin
		idle = 1;
		callstate = 2'b0;
		state = 2'b0;
		
	end
	else begin
		if (state == 2'b0 & (pay & money != 2'b0)) begin
			state = 2'b01;
			inputmoney <= money;
			idle = 0;
		end
		else if (state == 2'b01 & cancel) begin
			state = 2'b0;
			idle = 1;
		end
		else if (state == 2'b01 & call) begin
			state = 2'b10;
			idle = 0;
		end
		else if (state == 2'b10 & (cancel | money == 2'b0)) begin
			state = 2'b0;
			idle = 1;
		end
		
		if (state == 2'b10) begin
			if (callstate == 2'b0 & ~pay) begin
				callstate <= 2'b01;
			end
			else if (callstate == 2'b01 & ~pay) begin
				callstate <= 2'b01;
				inputmoney <= inputmoney -1;
			end
			else if (callstate == 2'b01 & (pay & money != 2'b0)) begin
				callstate <= 2'b10;
			end
			else if (callstate == 2'b10 & (pay & money != 2'b0)) begin
				callstate <= 2'b10;
				inputmoney <= inputmoney + (money - 1);
			end
			else if (callstate == 2'b01 & (cancel | money == 2'b0)) begin
				callstate <= 2'b0;
				state <= 2'b0;
				idle <= 1;
			end
			else if (callstate == 2'b10 & cancel) begin
				callstate <= 2'b0;
				state <= 2'b0;
				idle <= 1;
			end
			else if (callstate == 2'b10 & ~pay)
				callstate <= 2'b01;
			else if (callstate == 2'b0 & (pay & money != 2'b0)) begin
				callstate <= 2'b10;
			end
			else
				callstate = 2'b0;
		end
	end
end
endmodule

		
		
		
		
		
		