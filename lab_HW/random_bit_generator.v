// Copyright (C) 1991-2009 Altera Corporation
// Your use of Altera Corporation's design tools, logic functions 
// and other software and tools, and its AMPP partner logic 
// functions, and any output files from any of the foregoing 
// (including device programming or simulation files), and any 
// associated documentation or information are expressly subject 
// to the terms and conditions of the Altera Program License 
// Subscription Agreement, Altera MegaCore Function License 
// Agreement, or other applicable license agreement, including, 
// without limitation, that your use is for the sole purpose of 
// programming logic devices manufactured by Altera and sold by 
// Altera or its authorized distributors.  Please refer to the 
// applicable agreement for further details.

// PROGRAM		"Quartus II"
// VERSION		"Version 9.0 Build 132 02/25/2009 SJ Web Edition"
// CREATED ON	"Tue Apr 30 10:29:01 2019"

module random_bit_generator(
	clk,
	rstn,
	seed,
	out
);


input	clk;
input	rstn;
input	[4:0] seed;
output	out;

reg	a;
reg	b;
reg	c;
reg	d;
reg	e;
wire	f;
wire	[4:0] sn;
wire	SYNTHESIZED_WIRE_5;

assign	SYNTHESIZED_WIRE_5 = 1;




assign	sn[2] = rstn | seed[2];

assign	sn[1] = rstn | seed[1];

assign	sn[0] = rstn | seed[0];


always@(posedge clk or negedge SYNTHESIZED_WIRE_5 or negedge sn[4])
begin
if (!SYNTHESIZED_WIRE_5)
	begin
	e = 0;
	end
else
if (!sn[4])
	begin
	e = 1;
	end
else
	begin
	e = f;
	end
end


always@(posedge clk or negedge SYNTHESIZED_WIRE_5 or negedge sn[2])
begin
if (!SYNTHESIZED_WIRE_5)
	begin
	c = 0;
	end
else
if (!sn[2])
	begin
	c = 1;
	end
else
	begin
	c = d;
	end
end


always@(posedge clk or negedge SYNTHESIZED_WIRE_5 or negedge sn[0])
begin
if (!SYNTHESIZED_WIRE_5)
	begin
	a = 0;
	end
else
if (!sn[0])
	begin
	a = 1;
	end
else
	begin
	a = b;
	end
end


always@(posedge clk or negedge SYNTHESIZED_WIRE_5 or negedge sn[1])
begin
if (!SYNTHESIZED_WIRE_5)
	begin
	b = 0;
	end
else
if (!sn[1])
	begin
	b = 1;
	end
else
	begin
	b = c;
	end
end


always@(posedge clk or negedge SYNTHESIZED_WIRE_5 or negedge sn[3])
begin
if (!SYNTHESIZED_WIRE_5)
	begin
	d = 0;
	end
else
if (!sn[3])
	begin
	d = 1;
	end
else
	begin
	d = e;
	end
end

assign	f = a ^ c;

assign	sn[4] = rstn | seed[4];

assign	sn[3] = rstn | seed[3];

assign	out = f;

endmodule
