// Copyright (C) 1991-2009 Altera Corporation
// Your use of Altera Corporation's design tools, logic functions 
// and other software and tools, and its AMPP partner logic 
// functions, and any output files from any of the foregoing 
// (including device programming or simulation files), and any 
// associated documentation or information are expressly subject 
// to the terms and conditions of the Altera Program License 
// Subscription Agreement, Altera MegaCore Function License 
// Agreement, or other applicable license agreement, including, 
// without limitation, that your use is for the sole purpose of 
// programming logic devices manufactured by Altera and sold by 
// Altera or its authorized distributors.  Please refer to the 
// applicable agreement for further details.

// PROGRAM		"Quartus II"
// VERSION		"Version 9.0 Build 132 02/25/2009 SJ Web Edition"
// CREATED ON	"Tue Apr 30 10:15:24 2019"

module obstacle_generator(
	clk,
	rstn,
	pre,
	out
);


input	clk;
input	rstn;
input	[1:0] pre;
output	[1:0] out;

wire	down;
wire	downn;
wire	n0;
wire	n1;
wire	[1:0] out_ALTERA_SYNTHESIZED;
wire	[4:0] seed0;
wire	[4:0] seed1;
wire	up;
wire	upn;
wire	SYNTHESIZED_WIRE_0;
wire	SYNTHESIZED_WIRE_1;
wire	SYNTHESIZED_WIRE_2;
wire	SYNTHESIZED_WIRE_3;
wire	SYNTHESIZED_WIRE_8;
wire	SYNTHESIZED_WIRE_9;

assign	SYNTHESIZED_WIRE_8 = 1;
assign	SYNTHESIZED_WIRE_9 = 0;





random_bit_generator	b2v_inst1(
	.clk(clk),
	.rstn(rstn),
	.seed(seed0),
	.out(down));

assign	n1 =  ~pre[1];


random_bit_generator	b2v_inst11(
	.clk(clk),
	.rstn(rstn),
	.seed(seed1),
	.out(up));

assign	SYNTHESIZED_WIRE_0 = n1 & up;

assign	SYNTHESIZED_WIRE_1 = n0 & upn;

assign	out_ALTERA_SYNTHESIZED[0] = down & SYNTHESIZED_WIRE_0;

assign	out_ALTERA_SYNTHESIZED[1] = SYNTHESIZED_WIRE_1 & downn;


counter4	b2v_inst21(
	.clock(clk),
	.cnt_en(SYNTHESIZED_WIRE_2),
	.q(seed1[3:1]));


counter4	b2v_inst22(
	.clock(clk),
	.cnt_en(SYNTHESIZED_WIRE_3),
	.q(seed0[3:1]));

assign	SYNTHESIZED_WIRE_2 =  ~rstn;

assign	SYNTHESIZED_WIRE_3 =  ~rstn;

assign	seed1[4] = SYNTHESIZED_WIRE_8;


assign	seed0[0] = SYNTHESIZED_WIRE_8;


assign	seed1[0] = SYNTHESIZED_WIRE_9;


assign	seed0[4] = SYNTHESIZED_WIRE_9;



assign	upn =  ~up;

assign	downn =  ~down;

assign	n0 =  ~pre[0];

assign	out = out_ALTERA_SYNTHESIZED;

endmodule
