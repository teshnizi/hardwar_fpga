module lcdlab3(
  input CLOCK_50,    //    50 MHz clock
  input [17:0] SW,    //    Toggle Switch[17:0]
//    LCD Module 16X2
  output LCD_ON,    // LCD Power ON/OFF
  output LCD_BLON,    // LCD Back Light ON/OFF
  output LCD_RW,    // LCD Read/Write Select, 0 = Write, 1 = Read
  output LCD_EN,    // LCD Enable
  output LCD_RS,    // LCD Command/Data Select, 0 = Command, 1 = Data
  inout [7:0] LCD_DATA    // LCD Data bus 8 bits
);


wire [6:0] myclock;

// reset delay gives some time for peripherals to initialize
wire DLY_RST;
Reset_Delay r0(    .iCLK(CLOCK_50),.oRESET(DLY_RST) );

// turn LCD ON
assign    LCD_ON        =    1'b1;
assign    LCD_BLON    =    1'b0;

wire player_v;
wire [3:0] player_h;
wire [15:0] obs0, obs1;
assign player_v = 1'b1;
assign player_h = 4'b1010;
assign obs0 = 16'b1111111111111111;
assign obs1 = 16'b0011010010000111;


LCD_Display u1(
// Host Side
   .iCLK_50MHZ(CLOCK_50),
   .iRST_N(DLY_RST),
   .obs0(obs0),
   .obs1(obs1),
   .player_v(player_v),
   .player_h(player_h),
// LCD Side
   .DATA_BUS(LCD_DATA),
   .LCD_RW(LCD_RW),
   .LCD_E(LCD_EN),
   .LCD_RS(LCD_RS)
);


endmodule
